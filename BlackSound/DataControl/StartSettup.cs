﻿using System;
using System.IO;
using System.Data.Sql;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using MySql.Data.MySqlClient;

using BlackSound.Data;

namespace BlackSound.DataControl {
	public static class StartSettup {
		//izod servis
		public static bool flag = true;
		public static string database = "BlackSound";
		public static string connectionString = "Data Source=127.0.0.1;database="+database+";uid=root; pwd=password;SslMode=none;";

		public static void GenerateDefault() {
			if (flag) {
				flag = false;
				CreateTables();
				FillDefault();
			}
		}

		public static void CreateTables(){
			MySqlConnection connection = new MySqlConnection(connectionString);
			MySqlCommand command = new MySqlCommand("",connection);

			connection = new MySqlConnection("Data Source=127.0.0.1;uid=root;pwd=password;SslMode=none;");
			connection.Open();

			command = new MySqlCommand("create database if not exists BlackSound;", connection);
			command.ExecuteNonQuery();

			connection.Clone();

			using (connection = new MySqlConnection(connectionString)){
				
				try {
					connection.Open();
				}catch(MySqlException ex){
					Console.WriteLine("There was an internal error.");
					Console.WriteLine("Press any key to exit.");
					Console.ReadKey();
				}

				command=new MySqlCommand("use "+database+";",connection);
				command.ExecuteNonQuery();
				
				command = new MySqlCommand("create table if not exists Users(" +
				                           "id MEDIUMINT primary key auto_increment," +
				                           "name char(20) not null," +
				                           "password char(20) not null," +
				                           "email char(20) not null," +
				                           "isadmin tinyint not null);", connection);
				command.ExecuteNonQuery();

				command = new MySqlCommand("create table if not exists Songs(" +
											"id MEDIUMINT primary key auto_increment," +
				                           "name char(20) not null," +
				                           "artist char(20) not null," +
				                           "year char(20) not null);", connection);
				command.ExecuteNonQuery();

				command = new MySqlCommand("create table if not exists Playlists(" +
				                           "id MEDIUMINT primary key auto_increment," +
				                           "name char(20) not null," +
				                           "description char(50) not null," +
				                           "owner mediumint not null," +
				                           "ispublic tinyint not null," +
				                           "foreign key (owner) references Users(id) on delete cascade);", connection);
				command.ExecuteNonQuery();

				command = new MySqlCommand("create table if not exists Links(" +
										   "id MEDIUMINT primary key auto_increment," +
										   "firstid MEDIUMINT not null," +
										   "secondid MEDIUMINT not null" +
				                           ");",connection);
				command.ExecuteNonQuery();

				command = new MySqlCommand("create table if not exists Shares(" +
				                           "id MEDIUMINT primary key auto_increment," +
				                           "firstid MEDIUMINT not null," +
				                           "secondid MEDIUMINT not null," +
				                           "foreign key (firstid) references Users(id) on delete cascade," +
				                           "foreign key (secondid) references Playlists(id) on delete cascade);", connection);
				command.ExecuteNonQuery();
			}
			connection.Close();
		}

		public static void FillDefault(){
			User usr = new User("admin", "admin", "admin@localhost.net", true);

			Song[] songs = new Song[] {
				new Song("Sky fall","Adelle",new DateTime(2011,11,06)),
				new Song("Mama mia","ABBA",new DateTime(1995,08,26)),
				new Song("Here Come Revenge","Metallica",new DateTime(2014,03,28)),
				new Song("The Me in Me","Matt White",new DateTime(2015,09,17))
			};

			Playlist pl = new Playlist("All Songs", "All available songs.", 1, true);

			Repository<User> user = new Repository<User>();
			Repository<Song> song = new Repository<Song>();
			Repository<Playlist> playlists = new Repository<Playlist>();
			Repository<Link> ln = new Repository<Link>();

			if (user.GetAll().Count == 0) {
				user.Write(usr);
			}
			if (song.GetAll().Count == 0) {
				song.Write(songs.ToList());
			}
			if (playlists.GetAll().Count == 0) {
				playlists.Write(pl);
			}
			if (ln.GetAll().Count == 0) {
				for (int i = 0; i < songs.Length; i++) {
					ln.Write(new Link(1, i + 1));
				}
			}
		}
	}
}
