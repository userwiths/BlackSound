﻿using System;
using System.Collections.Generic;
using System.Linq;

using BlackSound.Data;

using PlaylistSong = System.
						 Collections.
						 Generic.
                           KeyValuePair<BlackSound.
						   Data.
						   Playlist, System.
										  Collections.
										  Generic.
                                           List<BlackSound.Data.Song>>;

namespace BlackSound.DataControl {
	public class UserController:Controller {
		private User currentUser;
		private SortedDictionary<int,PlaylistSong> userAvailablePlaylists;
		private SortedDictionary<int, PlaylistSong> ownedByUser;
		private PlaylistSong currentPlaylist;

		public User CurrentUser {
			get { return this.currentUser; }
		}

		public PlaylistSong CurrentPlaylist {
			get { return this.currentPlaylist; }
			set { this.currentPlaylist = value; }
		}

		public SortedDictionary<int,PlaylistSong> UserAvailable{
			get { return userAvailablePlaylists; }
		}

		public SortedDictionary<int,PlaylistSong> OwnedByUser{
			get{return ownedByUser;}
		}

		public UserController() :base(){
			userAvailablePlaylists = new SortedDictionary<int, PlaylistSong>();
			ownedByUser = new SortedDictionary<int, PlaylistSong>();
			if(currentUser==null){
				currentUser = Users.First().Value;
			}
		}

		public List<PlaylistSong> GetUserData(string name = "", string password = "") {

			if (name == "" || password == "") {
				name = CurrentUser.Name;
				password = CurrentUser.Password;
			}

			if (Users.Where(x => x.Value.Name == name && x.Value.Password == password).Count() != 0) {
				currentUser = Users.Where(x => x.Value.Name == name && x.Value.Password == password).First().Value;
			}

			List<PlaylistSong> result = new List<PlaylistSong>();
			result.AddRange(Playlists.
			                Where(x => x.Key.Owner == CurrentUser.ID ||
			                      x.Key.IsPublic ||
								  currentUser.IsAdmin));

			foreach (var item in Shared) {
				if (item.Key.Name == name) {
					result.Add(Playlists.Where(x=>x.Key.ID==item.Value.ID).First());
				}
			}

			return result;
		}

		public void ChangeUser(string name, string password) {
			if (Users.Where(x => x.Value.Name == name && x.Value.Password == password).Count() != 0) {
				userAvailablePlaylists = new SortedDictionary<int, PlaylistSong>();
				ownedByUser = new SortedDictionary<int, PlaylistSong>();

				currentUser = Users.Where(x => x.Value.Name == name && x.Value.Password == password).First().Value;

				LoadAvailable();

				LoadOwned();

				CurrentPlaylist = UserAvailable.First().Value;
			}else{
				currentUser = null;
			}
		}

		private void LoadAvailable(){
			foreach (var item in GetUserData()) {
				if(UserAvailable.ContainsKey(item.Key.ID)){
					continue;
				}
				UserAvailable.Add(item.Key.ID, item);
			}
		}

		private void LoadOwned(){
			foreach (var item in GetUserData()) {
				if (item.Key.Owner == CurrentUser.ID || CurrentUser.IsAdmin) {
					if (ownedByUser.ContainsKey(item.Key.ID)) {
						continue;
					}
					ownedByUser.Add(item.Key.ID, item);
				}
			}
		}

		public bool FirstRun() {
			return Users.Count == 1;
		}

		public void Restart(string name="",string pass="") {
			string uname = name!=""?name:currentUser.Name, passw =pass!=""? pass:currentUser.Password;

			LoadAllData();
			ChangeUser(uname, passw);
		}

		public int[] UserId(){
			return Users.Select(x => x.Key).ToArray();
		}

		public int[] SongId(){
			return Songs.Select(x => x.Key).ToArray();
		}

		public int[] CurrentSongId(){
			return CurrentPlaylist.Value.Select(x => x.ID).ToArray();
		}

		public int[] OwnedPlaylistId(){
			return OwnedByUser.Select(x=>x.Key).ToArray();
		}
	}
}
