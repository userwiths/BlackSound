﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.IO;
using System.Threading;

using BlackSound.Data;
using PlaylistSong = System.
						 Collections.
						 Generic.
                           KeyValuePair<BlackSound.
                           Data.
                           Playlist,System.
                                          Collections.
                                          Generic.
                                          List<BlackSound.Data.Song>>;

namespace BlackSound.DataControl {
	public class Controller {
		
		private SortedDictionary<int, User> users;
		private SortedDictionary<int, Song> songs;
		private SortedDictionary<int, PlaylistSong> playlist;
		private List<KeyValuePair<User, Playlist>> shared;

		public SortedDictionary<int, User> Users {
			get { return this.users; }
		}

		public SortedDictionary<int, Song> Songs {
			get { return this.songs; }
		}

		public List<PlaylistSong> Playlists {
			get { return this.playlist.Select(x=>x.Value).ToList(); }
		}

		public List<KeyValuePair<User, Playlist>> Shared{
			get { return this.shared; }
		}

		public Controller() {
			LoadAllData();
		}

		#region PRIVATE
		public void LoadAllData(){
			users = new SortedDictionary<int, User>();
			songs = new SortedDictionary<int, Song>();
			playlist = new SortedDictionary<int, PlaylistSong>();
			shared = new List<KeyValuePair<User, Playlist>>();

			Thread[] threads = new Thread[]{
				new Thread(new ThreadStart(LoadUsers)),
				new Thread(new ThreadStart(LoadSongs)),
				new Thread(new ThreadStart(LoadPlaylists)),
				new Thread(new ThreadStart(LoadShared))
			};

			foreach (var item in threads) {
				item.Start();
				item.Join();
			}
		}

		private void LoadUsers() {
			Repository<User> usr = new Repository<User>();

			foreach (var item in usr.GetAll()) {
				this.users.Add(item.ID, item);
			}
		}

		private void LoadSongs() {
			Repository<Song> songControler = new Repository<Song>();
			foreach (var item in songControler.GetAll()) {
				this.songs.Add(item.ID, item);
			}
		}

		private void LoadPlaylists() {
			Repository<Playlist> plControl = new Repository<Playlist>();

			List<Song> pl = new List<Song>();

			foreach (var item in plControl.GetAll()) {
				playlist.Add(item.ID, new PlaylistSong(item,LoadSongsInPlaylist(item.ID)));
			}
		}

		public List<Song> LoadSongsInPlaylist(int id){
			Repository<Link> link = new Repository<Link>();
			Repository<Song> song = new Repository<Song>();

			List<Song> result = new List<Song>();

			foreach (var item in link.GetAll()) {
				if(item.FirstId==id){
					if (song.GetById(item.SecondId) != null) {
						result.Add(song.GetById(item.SecondId));
					}
				}
			}

			return result;
		}

		private void LoadShared() {
			Repository<Share> contr = new Repository<Share>();
			Repository<User> user = new Repository<User>();
			Repository<Playlist> pl = new Repository<Playlist>();

			foreach (var item in contr.GetAll()) {
				if (user.GetById(item.FirstId) != null && pl.GetById(item.SecondId) != null) {
					this.shared.Add(new KeyValuePair<User, Playlist>(user.GetById(item.FirstId), pl.GetById(item.SecondId)));
				}
			}
		}
		#endregion

		#region USER_OPTIONS
		public void AddUser(User usr) {
			Repository<User> userControl = new Repository<User>();

			if (userControl.GetId(usr) == -1) {
				usr=userControl.ResponseWrite(usr);
			}

			if (!this.users.ContainsKey(usr.ID)) {
				this.users.Add(usr.ID, usr);
			}

		}

		public void RemoveUser(User usr) {
			Repository<User> userControl = new Repository<User>();
			Repository<Playlist> pl = new Repository<Playlist>();

			if (userControl.GetId(usr) != -1) {
				userControl.DeleteItem(usr);
			}

			if (this.users.ContainsKey(usr.ID)) {
				this.users.Remove(usr.ID);
			}
		}

		public void ReplaceUser(User old,User nw){
			Repository<User> ctrl = new Repository<User>();
			this.users[old.ID] = nw;
			ctrl.Replace(old,nw);
		}
		#endregion

		#region SONG_OPTIONS
		public void AddSong(Song sng) {
			Repository<Song> sngControl = new Repository<Song>();

			if (sngControl.Get(sng) == null) {
				sng = sngControl.ResponseWrite(sng);
			}

			if (!this.songs.ContainsKey(sng.ID)) {
				this.songs.Add(sng.ID, sng);

				if (!this.playlist[1].Value.Contains(sng)) {
					this.AddToPlaylist(1, sng);
				}
			}
		}

		public void RemoveSong(Song sng){
			Repository<Song> sngControl = new Repository<Song>();
			Repository<Link> ln = new Repository<Link>();

			if(sngControl.GetId(sng)!=-1){
				sng = sngControl.Get(sng);
			}

			if (this.songs.ContainsKey(sngControl.GetId(sng))) {
				this.songs.Remove(sng.ID);
			}

			sngControl.DeleteItem(sng);
		}

		public void ReplaceSong(Song old,Song nw){
			Repository<Song> ctrl = new Repository<Song>();

			if (this.songs.ContainsKey(old.ID)) {
				this.songs[old.ID]=nw;
				ctrl.Replace(old,nw);
			}

		}
#endregion

		#region PLAYLIST_OPTIONS
		public void AddPlaylist(Playlist pl) {
			Repository<Playlist> playControl = new Repository<Playlist>();

			if(playControl.GetId(pl)==-1){
				pl = playControl.ResponseWrite(pl);
			}

			if (this.playlist.Where(x => x.Value.Key.Equals(pl)).Count() == 0) {
				this.playlist.Add(pl.ID, new PlaylistSong(pl, new List<Song>()));
			}
		}

		public void RemovePlaylist(Playlist pl){
			Repository<Playlist> plControl = new Repository<Playlist>();
			Repository<Link> ln = new Repository<Link>();
			Repository<Share> sh = new Repository<Share>();

			pl = plControl.Get(pl);

			if (this.playlist.Where(x => x.Value.Key.Equals(pl)).Count() != 0) {
				this.playlist.Remove(pl.ID);
				plControl.DeleteItem(pl);
			}

		}

		public void ReplacePlaylist(Playlist old,Playlist nw){
			Repository<Playlist> plControl = new Repository<Playlist>();
			if (this.playlist.Where(x => x.Value.Key.Equals(old)).Count() != 0) {
				this.playlist[old.ID] = new PlaylistSong(nw,this.Playlists[old.ID].Value);
				plControl.Replace(old, nw);
			}
		}

		public void AddToPlaylist(int playlistId, Song sng) {
			PlaylistSong playlist = new PlaylistSong();
			Repository<Link> ln = new Repository<Link>();
			Repository<Song> sngC = new Repository<Song>();

			sng.ID = sngC.GetId(sng);

			if(sng.ID==-1){
				sng = sngC.ResponseWrite(sng);
			}

			if (this.playlist.ContainsKey(playlistId)) {
				this.playlist.TryGetValue(playlistId, out playlist);
				playlist.Value.Add(sng);

				ln.Write(new Link(playlistId,sng.ID));
			}
		}

		public void RemoveFromPlaylist(int playlistId,Song sng){
			PlaylistSong playlist = new PlaylistSong();
			Repository<Link> ln = new Repository<Link>();
			Repository<Song> sngC = new Repository<Song>();

			sng = sngC.Get(sng);

			if (this.playlist.ContainsKey(playlistId)) {
				this.playlist.TryGetValue(playlistId, out playlist);
				ln.DeleteItem(new Link(playlistId, sng.ID));
			}
		}

		public void SharePlaylist(Playlist pl,User sr){
			Repository<Share> sh = new Repository<Share>();
			this.shared.Add(new KeyValuePair<User, Playlist>(sr,pl));
			sh.Write(new Share(sr.ID,pl.ID));
		}
#endregion
	}
}
