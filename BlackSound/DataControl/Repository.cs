﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;

//NUGET

//xod
//BreezDB

using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data.OleDb;

using MySql.Data.MySqlClient;

using System.IO;
using System.Threading;

using BlackSound.Data;
using System.Reflection;

//REPOSITORY
namespace BlackSound.DataControl {
	public class Repository<T> where T:Base,new() {
		private static MySqlCommand command;
		private static MySqlConnection connection;

		public static MySqlConnection Connection{
			get { return connection; }
		}

		public static MySqlCommand Command{
			get { return command; }
		}

		public Repository() {
			StartSettup.GenerateDefault();
		}

		public T Create(object[] data){
			T result = new T();
			var field = result.GetType().GetProperties()[0];

			if(data.Length>result.GetType().GetProperties().Length){
				return null;
			}

			for (int i = 0; i < data.Length; i++) {
				field = result.GetType().GetProperties()[i];
				TypeWiseSet(result,field.Name,data[i]);
			}
			return result;
		}

		public T Create(MySqlDataReader data){
			T result = new T();
			var field = result.GetType().GetProperties()[0];

			for (int i = 0; i < data.FieldCount; i++) {
				field = result.GetType().GetProperties()[i];
				TypeWiseSet(result,field.Name,data[field.Name.ToLower()]);
			}

			return result;
		}

		private void TypeWiseSet(T instance,string propertyName,object data){
			var field = instance.GetType().GetProperties().
			                    Where(x => x.Name.ToLower() == propertyName.ToLower()).
			                    First();
			if(field==null){
				return;	
			}

			if (field.GetValue(instance) is DateTime) {
				field.SetValue(instance, DateTime.ParseExact(data.ToString(), "dd/MM/yyyy", null));
			} else if (field.GetValue(instance) is string) {
				field.SetValue(instance, data.ToString());
			} else if(field.GetValue(instance) is int){
				field.SetValue(instance, int.Parse(data.ToString()));
			}else{
				field.SetValue(instance,int.Parse(data.ToString())==1);
			}
		}

		public string GetQueryType(T instance, string fieldName) {
			string result = "";
			var property = instance.GetType().GetProperties().
							   Where(x => x.Name.ToLower() == fieldName.ToLower()).
							   First();

			if (property == null) {
				return result;
			}

			if (property.PropertyType == typeof(int)) {
				result = property.GetValue(instance).ToString();
			} else if (property.PropertyType == typeof(DateTime)) {
				result = "'" + DateTime.Parse(property.GetValue(instance).ToString()).ToString("dd/MM/yyyy") + "'";
			} else if (property.PropertyType == typeof(string)){
				result = "'" + property.GetValue(instance).ToString() + "'";
			}else{
				result = (bool.Parse(property.GetValue(instance).ToString())?1:0).ToString();
			}

			return result;
		}

		public void Write(T data){
			string com = "insert into ";
			com += GetTableName() + "(";
			com += string.Join(",",
			                   data.GetType().
			                   GetProperties().
			                   Where(x=>x.Name.ToLower()!="id").
			                   Select(y=>y.Name.ToLower()));

			com += ") values(";
			com += string.Join(",",
							   data.GetType().
							   GetProperties().
			                   Where(x => x.Name.ToLower() != "id").
			                   Select(y => GetQueryType(data,y.Name)));

			com += ");";

			using (connection = new MySqlConnection(StartSettup.connectionString)) {
				connection.Open();
				using (command = new MySqlCommand(com, connection)) {
					command.ExecuteNonQuery();
				}
			}
		}

		public T ResponseWrite(T data){
			if(Get(data)==null){
				Write(data);
			}

			data = Get(data);

			return data;
		}

		public void Write(List<T> data){
			foreach (var item in data) {
				Write(item);
			}
		}

		public void Write(string query) {
			using (connection = new MySqlConnection(StartSettup.connectionString)) {
				connection.Open();
				using (command = new MySqlCommand(query,connection)) {
					command.ExecuteNonQuery();
				}
			}

		}

		public string GetTableName(){
			T temp = new T();
			return temp.GetType().Name + "s";
		}

		public List<T> GetAll(){
			List<T> result=new List<T>();
			MySqlDataReader reader = null;
			using (connection = new MySqlConnection(StartSettup.connectionString)) {
				connection.Open();
				using (command = new MySqlCommand("select * from "+GetTableName()+";", connection)) {
					reader = command.ExecuteReader();
					while(reader.Read()){
						result.Add(Create(reader));
					}
				}
			}

			return result;
		}

		public T Get(T item) {
			T temp = new T();
			string comm="";

			List<string> compare = new List<string>();
			foreach (var elem in item.GetType().GetProperties()) {
				if (elem.Name.ToLower() == "id") {
					continue;
				}
				compare.Add(elem.Name.ToLower() + "=" + GetQueryType(item, elem.Name));
			}

			comm += string.Join(" and ", compare);
			comm += ";";

			using (connection = new MySqlConnection(StartSettup.connectionString)) {
				connection.Open();
				using (command = new MySqlCommand("select * from " + GetTableName() + " where " +comm, connection)) {
					var result = command.ExecuteReader();
					if (!result.Read()) {
						return null;
					}
					temp = Create(result);
				}
			}
			return temp;
		}

		public T GetById(int id){
			T temp = new T();
			using (connection = new MySqlConnection(StartSettup.connectionString)) {
				connection.Open();
				using (command = new MySqlCommand("select * from " + GetTableName() + " where id=" + id + ";", connection)) {
					var result = command.ExecuteReader();
					if(!result.Read()){
						return null;
					}
					temp = Create(result);
				}
			}
			return temp;
		}

		public int GetId(T item) {
			string	comm="select id from " + GetTableName() + " where ";
			int result = -1;
			List<string> compare = new List<string>();
			foreach (var elem in item.GetType().GetProperties()) {
				if (elem.Name.ToLower() == "id") {
					continue;
				}
				compare.Add(elem.Name.ToLower() + "=" + GetQueryType(item, elem.Name));
			}
			comm += string.Join(" and ", compare);
			comm += ";";
			using (connection = new MySqlConnection(StartSettup.connectionString)) {
				connection.Open();
				using (command = new MySqlCommand(comm, connection)) {
					var data = command.ExecuteReader();
					if(data.Read()){
						result = data.GetInt32(0);
						data.Close();
					}
				}
			}
			return result;
		}

		public void Verify(T item){
			if(item.ID!=0){
				return;
			}else{
				item = Get(item);
			}
		}

		public bool HasDublicate(T instance,string[] fields){
			bool result = false;
			using (connection = new MySqlConnection(StartSettup.connectionString)) {
				string query = "select * from " + GetTableName() + " where ";

				query += string.Join(" and ", fields.Select(x => x + "=" + GetQueryType(instance, x)));

				using(command=new MySqlCommand(query,connection)){
					var data = command.ExecuteReader();
					result = data.Read();
				}
			}
			return result;
		}

		public void DeleteItem(T item){
			string comm = "delete from " + GetTableName() + " where ";
			List<string> compare = new List<string>();
			foreach (var elem in item.GetType().GetProperties()) {
				if (elem.Name.ToLower() == "id") {
					continue;
				}
				compare.Add(elem.Name.ToLower() + "=" + GetQueryType(item, elem.Name));
			}
			comm += string.Join(" and ", compare);
			comm += ";";
			using (connection = new MySqlConnection(StartSettup.connectionString)) {
				connection.Open();
				using(command = new MySqlCommand(comm, connection)){
					command.ExecuteNonQuery();
				}
			}
		}

		public void Replace(T old,T nw){
			Verify(old);
			nw.ID = old.ID;

			string comm = "update " + GetTableName() + " set ";
			comm += string.Join(",",
			                    nw.GetType().
			                    GetProperties().
			                    Select(
				                    item=>
				                    item.Name.ToLower() +
				                    "=" + 
				                    GetQueryType(nw,item.Name)));

			comm += " where id=" + old.ID+";";
			Write(comm);
		}

		public void DeleteTable(){
			string comm = "drop table " + GetTableName() + ";";
			using (connection = new MySqlConnection(StartSettup.connectionString)) {
				connection.Open();
				using (command = new MySqlCommand(comm, connection)) {
					command.ExecuteNonQuery();
				}
			}
		}

		public void CreateTable(){
			string comm = "create table if not exists " + GetTableName() + "(id integer primary key autoincrement, ";
			T instance = new T();
			var property = instance.GetType().GetProperties();

			if (property.Count() == 0) {
				return;
			}

			foreach (var item in property) {
				comm += item.Name.ToLower() + " ";
				if (item.PropertyType == typeof(int)) {
					comm += "integer, ";
				} else if (item.PropertyType == typeof(bool)) {
					comm += "bool, ";
				}else{
					comm += "text, ";
				}
			}

			comm.TrimEnd(',');
			comm += ");";

			using (connection = new MySqlConnection(StartSettup.connectionString)) {
				using (command = new MySqlCommand(comm, connection)) {
					command.ExecuteNonQuery();
				}
			}
		}

		public void Recreate(){
			DeleteTable();
			CreateTable();
		}
	}
}
