﻿using System;
using System.Collections.Generic;

namespace BlackSound
{
    internal class Program
    {
        public static void Main(string[] args)
        {
			View view = new View();
			view.Show();
        }
    }
}