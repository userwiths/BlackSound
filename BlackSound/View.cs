﻿using System;
using System.Collections.Generic;
using System.Linq;

using BlackSound.DataControl;
using BlackSound.Data;

namespace BlackSound {
	public class View:UserController {
		public View():base() {
			
		}

		#region MISC
		public void Show(){
			if (FirstRun()) {
				ChangeUser("admin", "admin");
			} else {
				LoginScreen();
			}
			MainMenu();
			this.Restart();
		}

		public  void Template() {
			string message = "";
			Console.Clear();
			if (CurrentUser.IsAdmin) {
				message = "Whats up " + CurrentUser.Name + " here to have a good time or to take a break from a hard one?";
			} else {
				message = "Hello, " + CurrentUser.Name + " I hope you had a pleasant day.";
			}

			message += "\n\nPlaylist '"+CurrentPlaylist.Key.Name+"' is currently loaded.\n";

			message += "\nDo not forget tocheck your email: " + CurrentUser.Email;

			Console.WriteLine(message + "\n");
		}

		//Display options and return the users chooce.
		public int OptionList(string[] options,int[] corespondent=null){
			Console.WriteLine();
			if (options[0][0] >= '0' && options[0][0] <= '9') {
				foreach (var item in options) {
					Console.WriteLine(item);
				}
			} else {
				for (int i = 0; i < options.Length; i++) {
					Console.WriteLine("{0}. {1}.", i + 1, options[i]);
				}
			}

			int result = 0;
			if (int.TryParse(Console.ReadLine(),out result)) {
				if (result>=0 && result <= options.Length) {
					if(result==0){
						return 0;
					}
					if (corespondent != null) {
						return corespondent[result - 1];
					}
					return result;
				}
			}
			Console.WriteLine("Please enter a valid choice, or 0 to exit.");
			return OptionList(options,corespondent);
		}
		#endregion

		#region ACTIONS
		public User CreateUser() {
			string username, pass, email, admin=CurrentUser.IsAdmin.ToString().ToLower();

			Template();
			Console.WriteLine("\n\n\n");

			Console.Write("Enter user name: ");
			username = Console.ReadLine();

			Console.Write("Enter password: ");
			pass = Console.ReadLine();

			Console.Write("Enter Email: ");
			email = Console.ReadLine();

			if (CurrentUser.IsAdmin) {
				do {
					Console.Write("Give admin rights (yes/no): ");
					admin = Console.ReadLine();
				} while (admin != "yes" && admin != "no");

				if (username == "" || pass == "" || email == "") {
					Console.WriteLine("Invalid User Data.");
					Console.Write("Please enter : ");
					Console.WriteLine("User Name\tPassword\tEmail\tAdmin rights(yes/no)");
					Console.WriteLine("Pres any key to try again.");
					Console.ReadKey();
					return CreateUser();
				}
			}

			return new User(username, pass, email, admin.Contains("y")||admin.Contains("tr"));
		}

		public void DeleteUser(){
			int result = OptionList(Users.Select(x => x.Value.Name + "\t"+x.Value.Email).ToArray(),
			                        UserId());
			RemoveUser(Users[result]);
		}

		public void ViewUsers(){
			foreach (var item in Users) {
				Console.WriteLine(string.Format("ID: {0}\nName: {1}\nEmail: {2}\nIs Admin: {3}\n",item.Key,item.Value.Name,item.Value.Email,item.Value.IsAdmin));
			}
			Console.WriteLine("Press any key to continue.");
			Console.ReadKey();
		}

		#region SONG_OPTIONS
		public  void EditSong() {
			Console.WriteLine("Enter the number of the song you wish to edit: ");
			int index = OptionList(CurrentPlaylist.Value.Select(x=>x.Name+"\t"+x.Artist).ToArray()
			                       ,CurrentSongId());
			if (index == 0) { return; }
			Song nw = CreateSong(CurrentPlaylist.Value[index-1]);
			ReplaceSong(CurrentPlaylist.Value[index], nw);
		}

		public void RemoveSong() {
			Console.Write("Enter number of song to remove: ");
			int index = int.Parse(Console.ReadLine());
			if (CurrentPlaylist.Key.ID == 1) {
				RemoveSong(CurrentPlaylist.Value.Where(x => x.ID == index).First());
			}else{
				RemoveFromPlaylist(CurrentPlaylist.Key.ID,CurrentPlaylist.Value.Where(x => x.ID == index).First());
			}
		}

		public  void AddSong() {
			var songs = AddSongMenu();

			if(songs==null){
				return;
			}

			foreach (var item in songs) {
				AddToPlaylist(CurrentPlaylist.Key.ID,item);
			}
			CurrentPlaylist.Value.AddRange(songs);
		}

		public Song CreateSong(Song old = null) {
			string name="", year="", art="";
			string temp = "";
			Song song = new Song();

			if (old == null) {
				Console.Write("Name: ");
				name = Console.ReadLine();
				Console.Write("Artist: ");
				art = Console.ReadLine();
				Console.Write("Year(dd/MM/yyyy): ");
				year = Console.ReadLine();
			}else{
				name = old.Name;
				art = old.Artist;
				year = old.Year.ToString("dd/MM/yyyy");

				Console.Write("Name ("+name+"): ");
				temp = Console.ReadLine();
				if(temp!=""){
					name = temp;
				}

				Console.Write("Artist("+art+"): ");
				temp = Console.ReadLine();
				if (temp != "") {
					art = temp;
				}

				Console.Write("Year("+year+"): ");
				temp = Console.ReadLine();
				if (temp != "") {
					year = temp;
				}
			}

			if(name=="" || art==""){
				Console.WriteLine("PLease enter a title and an artist");
				song = CreateSong(old);
			}else{
				song = new Song(name, art, DateTime.ParseExact(year, "dd/MM/yyyy", null));
			}

			return song;

		}
		#endregion

		#region PLAYLIST_OPTIONS
		public void SharePlaylist(){
			int pl=0,usr=0;

			Console.WriteLine("Choose the playlist you wish to share (0 to exit): ");
			Console.WriteLine();
			pl=OptionList(OwnedByUser.Select(x=>x.Value.Key.Name+"\t"+x.Value.Key.Description.Split('\n')[0]).ToArray(),
			              OwnedPlaylistId());

			if(pl==0){
				return;
			}

			Console.WriteLine();

			Console.WriteLine("Choose user to share with (0 to exit): ");
			Console.WriteLine();
			usr = OptionList(Users.Select(x => x.Value.Name).ToArray(),Users.Select(x => x.Key).ToArray());
			if(usr==0){
				return;
			}
			Console.WriteLine();

			SharePlaylist(OwnedByUser.Where(x=>x.Key==pl).First().Value.Key,
			              Users.Where(x=>x.Key==usr).First().Value);
		}

		public void CreatePlaylist(){
			string name="", description="", ispublic="";
			string temp = "";
			Playlist pl = null; 

			Console.Write("Enter Playlist name: ");
			name = Console.ReadLine();

			Console.Write("Enter description (enter an empty line to stop): ");
			do {
				description += temp+"\n";
				temp = Console.ReadLine();
			} while (temp != "");

			description = description.Substring(1);

			Console.Write("Is it public (yes/no): ");
			do {
				ispublic = Console.ReadLine();
			} while (ispublic!="yes" && ispublic!="no");

			pl=new Playlist(name,description,CurrentUser.ID,ispublic=="yes");

			AddPlaylist(pl);
		}

		public void DeletePlaylist(){

			Console.WriteLine();
			int result = OptionList(OwnedByUser.Select(x => x.Value.Key.Name).ToArray(),
			                        OwnedByUser.Select(x => x.Value.Key.ID).ToArray());
			Console.WriteLine();

			string temp = "";

			if(result==0){
				return;
			}

			Console.Write("Are you sure you wish to delete '{0}' (yes/no): ",OwnedByUser[result].Key.Name);
			do {
				temp = Console.ReadLine();
			} while (temp!="yes" && temp!="no");

			if(temp.ToLower().Contains("y")||temp.ToLower().Contains("tr")){
				RemovePlaylist(OwnedByUser[result].Key);
				CurrentPlaylist = OwnedByUser.First().Value;
			}
		}

		public void EditName(){
			Console.WriteLine("Old: "+CurrentPlaylist.Key.Name);
			Console.Write("New: ");
			CurrentPlaylist.Key.Name = Console.ReadLine();
		}

		public void EditDescription() {
			string temp = "";
			Console.WriteLine("Old:\n" + CurrentPlaylist.Key.Description);
			Console.Write("New:\n");
			CurrentPlaylist.Key.Description = "";
			do {
				temp = Console.ReadLine();
				CurrentPlaylist.Key.Description += temp + "\n";
			} while (temp != "");
		}

		public void EditUser(){
			User newUser = CreateUser();

			ReplaceUser(CurrentUser,newUser);
			Console.Clear();
		}


#endregion

		#endregion

		#region MENUS
		public void LoginScreen() {
			Console.Write("User Name: ");
			var name = Console.ReadLine();
			Console.Write("Password: ");
			var pass = Console.ReadLine();

			if (name == "quit" && pass == "quit") {
				Environment.Exit(0);
			}

			ChangeUser(name, pass);
			if (CurrentUser == null) {
				Console.WriteLine("There is no such user in the system :(");
				Console.WriteLine("Enter 'quit' for user & password to exit.");
				LoginScreen();
			}
		}

		public void MainMenu() {
			List<string> actions = new List<string>();
			int result = 0;

			actions.AddRange(new string[] { "Log out","View Playlists.", "Create new Playlist.", "Delete Playlist", "Share Playlist.","Edit own info." });
			if (CurrentUser.IsAdmin) {
				actions.Add("View Users.");
				actions.Add("Create User.");
				actions.Add("Delete User.");
				actions.Add("Create Song.");
			}

			while (true) {
				Template();
				Console.WriteLine("0. Exit");

				result = OptionList(actions.ToArray());

				switch (result) {
					case 1: LoginScreen();MainMenu();break;
					case 2: OutsidePlaylistMenu();break;
					case 3: CreatePlaylist();Restart();break;
					case 4: DeletePlaylist();Restart();break;
					case 5: SharePlaylist();Restart();break;
					case 6: EditUser();Restart();LoginScreen();MainMenu();break;
					case 7:
						if (CurrentUser.IsAdmin) {
							ViewUsers();
						}
						break;
					case 8:
						if (CurrentUser.IsAdmin) {
							AddUser(CreateUser());Restart();
						}
						break;
					case 9:
						if (CurrentUser.IsAdmin) {
							DeleteUser();Restart();
						}
						break;
					case 10:
						AddSong(CreateSong());Restart();
						break;
					case 0: Environment.Exit(0);break;
					default: return;
				}
			}
		}

		public void InsidePlaylistMenu() {
			Template();
			List<string> options = new List<string>();
			int result = 0;

			Console.WriteLine("\n\nName: {0}\tOwner: {1}\tPublic: {2}\nDescription:\n{3}\n",
			                  CurrentPlaylist.Key.Name,
			                  CurrentPlaylist.Key.Owner,
			                  CurrentPlaylist.Key.IsPublic.ToString(),
			                  CurrentPlaylist.Key.Description);
			PrintPlaylist();

			if(CurrentUser.ID==CurrentPlaylist.Key.Owner || CurrentUser.IsAdmin){
				options.AddRange(new string[] { "Add Song.", "Remove Song", "Edit Song", "Edit Name", "Edit Description","Share Playlist" });
			}
			if(CurrentUser.IsAdmin){
				options.Add("Create song.");
			}

			if (options.Count == 0) {
				Console.WriteLine("You can only view this playlist.");
				Console.WriteLine("Press any key to continue.");
				Console.ReadKey();
			} else {
				result = OptionList(options.ToArray());

				switch (result) {
					case 1: AddSong();Restart(); break;
					case 2: RemoveSong();Restart(); break;
					case 3: EditSong();Restart(); break;
					case 4: EditName();Restart(); break;
					case 5: EditDescription();Restart(); break;
					case 6: SharePlaylist();Restart(); break;
					case 7: var sng=CreateSong();
							if(sng!=null){
								//AddSong(sng);
								AddToPlaylist(CurrentPlaylist.Key.ID,sng);
								Restart();
							}
							break;
					default: return;
				}
			}
		}

		public  void OutsidePlaylistMenu() {
			Template();
			Console.WriteLine("\n\n");

			Console.WriteLine();
			Console.Write("Enter a number to continue (0 = main menu): ");

			int chooce = OptionList(UserAvailable.Select(item => item.Value.Key.Name + "\t" +
											  item.Value.Key.Description.Split('\n')[0]).ToArray(),
					   UserAvailable.Select(item => item.Key).ToArray()
					  );

			if (chooce != 0 ){
				CurrentPlaylist=UserAvailable[chooce];
				InsidePlaylistMenu();
			}
		}

		public void PrintPlaylist() {
			Console.WriteLine("\nSongs:");

			foreach (var item in CurrentPlaylist.Value) {
				Console.WriteLine("\t{0}\t{1}\t{2}\t{3}", item.ID, item.Name, item.Artist, item.Year);
			}
			Console.WriteLine();
		}

		public  void ViewAllSongs() {
			CurrentPlaylist = UserAvailable.Where(x => x.Key == 1).First().Value;
			InsidePlaylistMenu();
		}

		public List<Song> AddSongMenu(){
			Template();
			Console.WriteLine("\n\n\n");

			List<Song> result = new List<Song>();
			var allSongs = Songs;
			int temp;
			Console.WriteLine("Please enter the number of the songs you wish to include (0 exit): ");
			temp=OptionList(allSongs.Select(x => x.Value.Name + "\t" + 
			                                x.Value.Artist + "\t" + x.Value.Year).ToArray(),
			                SongId());

			if(temp==0){
				return null;
			}

			result.Add(allSongs[temp]);
			do {
				temp = int.Parse(Console.ReadLine());
				if(temp==0){
					break;
				}
				result.Add(allSongs[SongId()[temp-1]]);
			} while (temp!=0);
			return result;
		}
		#endregion

	}
}
