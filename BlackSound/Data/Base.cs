﻿using System;
using System.Collections.Generic;
//using System.Data.SQLite;
//using SQLTest.Data;

namespace BlackSound.Data {
	public class Base {
		private int id;

		public int ID{
			get { return id; }
			set { id = value; }
		}

		public Base() {
			this.id = 0;
		}

	}
}
