﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlackSound.Data {
	public class User :Base{

		private string name;
		private string password;
		private string email;
		private bool isAdmin = false;

		public string Password{ 
			get { return this.password; }
			set { this.password = value; }
		}

		public string Email{ 
			get { return this.email; }
			set { this.email = value; }
		}

		public string Name{ 
			get { return this.name; }
			set { this.name = value; }
		}

		public bool IsAdmin{
			get { return isAdmin; }
			set { isAdmin = value; }
		}

		public User(){
			this.Name = "Unknown";
			this.email = "Unknown";
			this.password = "Unknown";
			this.isAdmin = false;
		}

		public User(string name,string password,string email="none",bool isAdmin=false) {
			this.name=name;
			this.password = password;
			this.email = email;
			this.isAdmin = isAdmin;
		}

		//Use less, did not work even one time??
		public override string ToString() {
			return string.Format("ID:{0}\nName:{1}\nEmail:{2}\nIs Admin:{3}\n", ID,Name,email,IsAdmin);
		}
	}
}
