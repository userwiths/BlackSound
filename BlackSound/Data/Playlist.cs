﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlackSound.Data {
	public class Playlist:Base {
		private string name;
		private string description;
		private int owner;
		private bool isPublic;

		public string Name{
			get { return this.name; }
			set { this.name = value; }
		}

		public string Description{
			get { return this.description; }
			set { this.description = value; }
		}

		public bool IsPublic{
			get { return this.isPublic; }
			set { this.isPublic = value; }
		}

		public int Owner{
			get { return this.owner; }
			set { this.owner = value; }
		}

		public Playlist(){
			this.Name = "Unknown";
			this.Description = "Unknown";
			this.Owner = 1;
			this.isPublic = false;
		}

		public Playlist(string name,string description,int owner,bool ispublic) {
			this.name = name;
			this.description = description;
			this.owner = owner;
			this.isPublic = ispublic;
		}

		public override bool Equals(object obj) {
			Playlist pl = obj as Playlist;
			if(pl==null){
				return false;
			}
			return (pl.Name == Name) &&
				(pl.Description == Description) &&
				(pl.Owner == Owner) &&
				(pl.IsPublic == IsPublic);
		}

		public override string ToString() {
			var result= string.Format("{0}\t{1}\t{2}\t{3}",name,description,isPublic.ToString(),Owner);
			return result;
		}
	}
}
