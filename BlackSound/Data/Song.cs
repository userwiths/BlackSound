﻿using System;
namespace BlackSound.Data {
	public class Song:Base{
		private string name;
		private string artist;
		private DateTime year;

		public string Name {
			get { return this.name; }
			set { this.name = value; }
		}

		public string Artist {
			get { return artist; }
			set { this.artist = value; }
		}

		public DateTime Year{
			get { return this.year; }
			set{this.year=value;}
		}

		public Song(){
			this.name = "Unknown";
			this.artist = "Unknown";
			this.year = new DateTime();
		}

		public Song(string name,string artist,DateTime time) {
			this.name = name;
			this.artist = artist;
			this.year = time;
		}

		public override bool Equals(object obj) {
			Song sng = obj as Song;
			if(sng==null){
				return false;
			}
			return sng.Name == Name && sng.Artist == Artist && sng.Year == Year;
		}

		public override string ToString() {
			return string.Format("ID:{0}\nName:{1}\nArtist:{2}\nYear:{3}\n",ID,name,artist,year.ToString("dd/MM/yyyy"));
		}
	}
}
