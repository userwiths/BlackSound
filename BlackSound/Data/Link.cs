﻿using System;
namespace BlackSound.Data {
	public class Link : Base {
		private int firstId;
		private int secondId;

		public int FirstId{
			get{return firstId;}
			set { firstId = value; }
		}

		public int SecondId{
			get { return secondId; }
			set { secondId = value; }
		}

		public Link(){
			firstId = 1;
			secondId = 1;
		}

		public Link(int a=1,int b=1) {
			firstId = a;
			secondId = b;
		}
	}
}
